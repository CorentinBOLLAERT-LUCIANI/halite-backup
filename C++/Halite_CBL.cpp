#include "hlt/game.hpp"
#include "hlt/constants.hpp"
#include "hlt/log.hpp"
#include <queue>

#include <random>
#include <ctime>
#include <algorithm>
#include <cmath>
#include <deque>
#include <queue>
#include <vector>
#include <set>
#include <unordered_set>
#include <map>

const int BUILDING_SPACING = 16;
const int NUMBER_OF_SHIP_FOR_FIRST_DROPPOFF = 8;
const int ADITIONAL_SHIPS_FOR_ADITIONAL_DROPOFF = 5;
const int SUDDEN_DEATH_GRACE_TURN = 15;
const float DEPLACEMENT_COST_FACTOR = 2;

using namespace std;
using namespace hlt;

enum class Purpose
{
    Stuck = 0,         // must stay put b/c too little halite
    Ram = 1,           // trying to hit an adjacent enemy
    EvadeReturner = 2, // getting out of the way of someone with the Return purpose
    Flee = 3,          // moving away from an adjacent enemy
    Return = 4,        // bringing halite back to base
    Mine = 5,          // getting halite
    Build = 6,         // going to build a dropoff
    SuddenDeath = 7,   // bringing halite back to base at the end of game
};

struct Plan
{
    shared_ptr<Ship> ship;
    Position tgt;
    Purpose purpose;
};

struct Bot
{
    // GLOBAL STATE
    vector<Plan> plans;                    // cleared each turn
    set<int> busy_ship_ids;                // ships that shouldn't be assigned new plans. Cleared each turn
    unordered_set<int> returning_ship_ids; // maintained across turns
    bool want_dropoff = false;             // recomputed by WantToBuildDropOff() each turn
    Position planned_dropoff_pos;
    map<Position, double> safety_map;
    bool still_spawning_ships = true;
    bool has_brut_force_return = false;

    vector<Command> turn() {}
};

int main(int argc, char *argv[])
{
    unsigned int rng_seed;
    if (argc > 1)
    {
        rng_seed = static_cast<unsigned int>(stoul(argv[1]));
    }
    else
    {
        rng_seed = static_cast<unsigned int>(time(nullptr));
    }
    mt19937 rng(rng_seed);
    Game game;

    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.

    game.ready("CBLBot");

    shared_ptr<Player> me = game.me;
    unique_ptr<GameMap> &game_map = game.game_map;

    vector<Plan> plans;
    vector<Plan> new_plans;

    //bool SUDDEN_DEATH = false; // if true, every ship will go back to the closest deposit

    int builder_ship_id;

    for (;;)
    {
        game.update_frame();
        //new_plans.clear();
        vector<Command> command_queue;

        // STARTING SHIP MANAGEMENT
        for (Plan p : plans)
        {
            for (const auto &ship_iterator : me->ships)
            {
                shared_ptr<Ship> ship = ship_iterator.second;

                // finding the plan's ship
                if (ship->id == p.ship->id)
                {
                    // if the ship hasn't reached its target yet
                    // its behavior will be processed after
                    if (ship->position.equals(p.tgt))
                        continue;

                    // if the ship hasn't moved
                    if (ship->position.equals(p.ship->position))
                    {
                        // moving it to a random adjacent tile
                        AddPlan(new_plans, ship, ship->position.get_surrounding_cardinals()[rng() % 4], Purpose::EvadeReturner);
                    }
                }
            }
        }

        // DETERMINING NEW SHIPS' GOAL

        bool want_dropoff = WantToBuildDropOff(game.turn_number, me->dropoffs.size(), me->ships.size());

        ManageReturningShips(plans, me, game, new_plans);

        ManageRemainingShips(me, game, want_dropoff, command_queue, new_plans);

        DefineNextTurnShipsBehavior(new_plans, game, command_queue);

        UpdatePlans(plans, new_plans);

        BuyShips(game, me, want_dropoff, command_queue);

        if (!game.end_turn(command_queue))
        {
            break;
        }
    }

    return 0;
}

// UTILITARY FUNCTIONS //

/**
 * @brief Main utilitary function. Handles returning ships behavior.
 * 
 * @param plans 
 * @param me 
 * @param game 
 * @param new_plans 
 */
void ManageReturningShips(std::vector<Plan> plans, shared_ptr<Player> me, Game &game, std::vector<Plan> &new_plans)
{
    for (Plan p : plans)
    {
        if (p.purpose == Purpose::Return)
        {
            for (const auto &ship_iterator : me->ships)
            {
                shared_ptr<Ship> ship = ship_iterator.second;

                if (ship->id == p.ship->id)
                {
                    Position closest_deposit = ClosestDeposit(ship->position, game);
                    // if the ship is still too full, go on
                    if (ship->halite > constants::MAX_HALITE / 2)
                    {
                        AddPlan(new_plans, ship, closest_deposit, p.purpose);
                    }
                }
            }
        }
    }
}

/**
 * @brief Main utilitary function. Handles the non-returning ships behavior.
 * 
 * @param me 
 * @param game 
 * @param want_dropoff 
 * @param command_queue 
 * @param new_plans 
 */
void ManageRemainingShips(shared_ptr<Player> me, Game &game, bool want_dropoff,
                          vector<Command> &command_queue, std::vector<Plan> &new_plans)
{
    Position best_droppoff_pos = BestDropOffPosition(game, me->shipyard->position, game.game_map->width / 3);

    for (const auto &ship_iterator : me->ships)
    {
        shared_ptr<Ship> ship = ship_iterator.second;

        Position closest_deposit_pos = ClosestDeposit(ship->position, game);
        int Shortest_time_to_deposit = game.game_map->calculate_distance(ship->position, closest_deposit_pos);

        Purpose new_purpose = Purpose::Mine;

        // END OF GAME COMING IN
        if (constants::MAX_TURNS - game.turn_number < SUDDEN_DEATH_GRACE_TURN + Shortest_time_to_deposit)
        {
            new_purpose = Purpose::SuddenDeath;
            // log::log("Pour la patrie !!! uwu");
        }
        // BUILDING A DROPOFF
        else if (want_dropoff && ClosestShipToPosition(game, best_droppoff_pos, me->ships)->position.equals(ship->position))
        {
            //log::log(ship->to_string() + "Je suis le plus proche");
            new_purpose = Purpose::Build;
        }
        // COMING BACK HOME
        else if (ship->is_full())
        {
            //log::log("Je dois rentrer avec " + to_string(ship->halite));
            new_purpose = Purpose::Return;
        }
        // MINING
        else if (ship->halite <= constants::MAX_HALITE / 100)
        {
            //log::log("ID : "+ to_string(ship->id) + " - Je dois aller miner ");
            new_purpose = Purpose::Mine;
        }

        // DEFINING BEST TARGET TO ACHIEVE GOAL
        Position target_pos;
        switch (new_purpose)
        {
        case Purpose::SuddenDeath:
            target_pos = closest_deposit_pos;
            break;

        case Purpose::Build:
            target_pos = best_droppoff_pos;

            // log::log(ship->to_string() + " going to build on : " + bestDroppoffPos.to_string());
            // log::log((game.game_map->at(bestDroppoffPos)->has_structure()) ? "There is something there" : "Spot clear");

            // if there's already a dropOff near, go mining
            if (game.game_map->at(best_droppoff_pos)->has_structure())
            {
                target_pos = GetBestHaliteMiningPosition(game, ship);
                new_purpose = Purpose::Mine;

                continue;
            }

            // if the ship reached the target position
            else if (best_droppoff_pos.equals(ship->position))
            {
                // it builds the dropOff if possible
                if (me->halite >= constants::DROPOFF_COST)
                {
                    command_queue.push_back(ship->make_dropoff());
                }
                // else, it waits
                continue;
            }

            break;

        case Purpose::Return:
            target_pos = closest_deposit_pos;
            break;

        case Purpose::Mine:
            target_pos = GetBestHaliteMiningPosition(game, ship);
            break;
        }

        // adding the ship's plan in the list
        AddPlan(new_plans, ship, target_pos, new_purpose);
    }
}

/**
 * @brief Main utilitary function. Handles the safety of the ships navigation individually.
 * 
 * @param new_plans 
 * @param game 
 * @param command_queue 
 */
void DefineNextTurnShipsBehavior(std::vector<Plan> &new_plans, Game &game, vector<Command> &command_queue)
{
    for (Plan p : new_plans)
    {
        //log::log("ID : " + to_string(p.ship->id) + " - Dest : " + p.tgt.to_string() + " - Purpose " + to_string((int)p.purpose));
        //log::log(p.ship->to_string()+ " - Purpose " + to_string((int)p.purpose) + " Going to : " + p.tgt.to_string());

        if (p.tgt.equals(p.ship->position))
        {
            continue;
        }

        bool risky_move = p.purpose == Purpose::SuddenDeath && game.game_map->calculate_distance(p.ship->position, p.tgt) <= 1;

        if (risky_move)
        {
            command_queue.push_back(p.ship->move(game.game_map->get_unsafe_moves(p.ship->position, p.tgt)[0]));
        }
        else
        {
            command_queue.push_back(p.ship->move(game.game_map->naive_navigate(p.ship, p.tgt)));
        }
    }
}

/**
 * @brief Main utilitary function. Handles whether or not the bot should spawn new ships.
 * 
 * @param game 
 * @param me 
 * @param want_dropoff 
 * @param command_queue 
 */
void BuyShips(Game &game, shared_ptr<Player> me, bool want_dropoff, vector<Command> &command_queue)
{
    if (
        game.turn_number <= 2 * constants::MAX_TURNS / 4 &&
        me->halite >= constants::SHIP_COST &&
        !game.game_map->at(me->shipyard)->is_occupied())
    {
        if (WantToBuildAShip(want_dropoff, me->halite))
        {
            command_queue.push_back(me->shipyard->spawn());
        }
    }
}

/**
 * @brief Main utilitary function. Manages the plans for the next turn.
 * 
 * @param plans 
 * @param new_plans 
 */
void UpdatePlans(std::vector<Plan> &plans, std::vector<Plan> &new_plans)
{
    plans.clear();
    for (Plan p : new_plans)
    {
        AddPlan(plans, p.ship, p.tgt, p.purpose);
    }
    new_plans.clear();
}

/**
 * @brief Add a plan to the list, linked to a ship
 * 
 * @param plans 
 * @param ship 
 * @param tgt the target position
 * @param purpose 
 */
void AddPlan(vector<Plan> &plans, shared_ptr<Ship> ship, Position tgt, Purpose purpose)
{
    for (Plan p : plans)
    {
        // managing duplicates
        if (p.ship->id == ship->id)
        {
            if (purpose == Purpose::SuddenDeath)
            {
                p.purpose = Purpose::SuddenDeath;
                p.tgt = tgt;
            }
            return;
        }
    }
    plans.push_back({ship, tgt, purpose});
}

/**
 * @brief Get the Best Halite Mining Position for a ship
 * 
 * @param game 
 * @param ship 
 * @return the targeted position
 */
Position GetBestHaliteMiningPosition(Game &game, std::shared_ptr<Ship> ship)
{
    int best_number_of_turn_to_get_full_and_drop = 999;
    Position best_halite_pos;

    int halite_missing = constants::MAX_HALITE - ship->halite;

// searching in map
    for (int x = 0; x <= game.game_map->width - 1; x++)
    {
        for (int y = 0; y <= game.game_map->height - 1; y++)
        {
            Position cell_pos = {x, y};
            MapCell *cell = game.game_map->at(cell_pos);

            // checking if amount of halite is worth
            if (cell->halite < game.game_map->at(ship->position)->halite)
            {
                continue;
            }

            int halite_per_turn = cell->halite / constants::EXTRACT_RATIO;
            if (halite_per_turn < 10)
                continue;

            int turns_to_get_full = (int)(halite_missing / halite_per_turn);

            //log::log("turnToGetFull " + to_string(turnToGetFull));

            // calculating the journey cost
            int turns_to_get_there = game.game_map->calculate_distance(cell_pos, ship->position);

            Position closest_deposit = ClosestDeposit(cell_pos, game);
            int closest_dist_to_dropOff = game.game_map->calculate_distance(cell_pos, closest_deposit);

            int turns_to_get_full_and_deliver = DEPLACEMENT_COST_FACTOR * turns_to_get_there + turns_to_get_full
             + 1 * closest_dist_to_dropOff;

            if (turns_to_get_full_and_deliver < best_number_of_turn_to_get_full_and_drop)
            {
                best_number_of_turn_to_get_full_and_drop = turns_to_get_full_and_deliver;
                best_halite_pos = cell_pos;
            }

            /*   log::log("TurnToGetFullAndDeliver " + to_string(TurnToGetFullAndDeliver));
               log::log("x " + to_string(x));
               log::log("y " + to_string(y));*/
        }
    }
    //log::log("BestNumberOfTurnToGetFullAndDrop " + to_string(BestNumberOfTurnToGetFullAndDrop));

    return best_halite_pos;
}

/**
 * @brief Get the Richest Cell in a given radius
 * 
 * @param game 
 * @param radius 
 * @return Position 
 */
Position GetRichestCell(Game &game, int radius)
{

    Position richest_cell_pos;

    double max_halite = 0;

    for (int x = game.me->shipyard->position.x - radius; x <= game.me->shipyard->position.x + radius; x++)
    {
        for (int y = game.me->shipyard->position.y - radius; y <= game.me->shipyard->position.y + radius; y++)
        {
            Position pos = {x, y};
            MapCell *cell = game.game_map->at(pos);
            if (cell->halite > max_halite)
            {
                max_halite = cell->halite;
                richest_cell_pos = cell->position;
            }
        }
    }
    return richest_cell_pos;
}

/**
 * @brief Returns wheter the bot can afford to buy a new ship or not.
 * Though, it favorises the construction of a dropOff if it is also planned.
 * 
 * @param halite 
 * @param want_dropoff 
 */
bool CanAffordAShip(int halite, bool want_dropoff)
{
    int min_halite_to_spawn = constants::SHIP_COST;

    // if we want to build a dropoff but we didn't manage to do it this turn, then
    // only build a ship if we'll have enough halite left after building the ship
    // to also build a dropoff
    if (want_dropoff)
        min_halite_to_spawn += constants::DROPOFF_COST;
    return halite >= min_halite_to_spawn;
}

/**
 * @brief  Returns if the bot wants to buy a ship.
 * Though it will favorise buying a dropOff if it is planned as well.
 * 
 * @param wantDroppOff 
 * @param halite
 * 
 * @return true if there is enough alite 
 */
bool WantToBuildAShip(bool want_droppOff, int halite)
{

    if (want_droppOff)
    {
        return (halite >= constants::DROPOFF_COST + constants::SHIP_COST);
    }
    return (halite >= constants::SHIP_COST);
}

/**
 * @brief Returns if the bot wants to buy a dropOff or not.
 * 
 * @param turn_number 
 * @param dropoffCount 
 * @param shipCount
 */
bool WantToBuildDropOff(int turn_number, int dropOff_count, int ship_count)
{

    int desired_number_of_dropOff = ceil((ship_count - NUMBER_OF_SHIP_FOR_FIRST_DROPPOFF) / ADITIONAL_SHIPS_FOR_ADITIONAL_DROPOFF);
    // log::log("DesiredNumberOfDropOff : " + to_string(DesiredNumberOfDropOff)+ " for "+to_string(shipCount)+" ships");

    return (dropOff_count < desired_number_of_dropOff);
}

/**
 * @brief Calculates the average amount of halite in a given radius.
 * 
 * @param game 
 * @param tilePosition 
 * @param radius 
 * @return int 
 */
int TileAverageHalite(Game &game, Position tile_position, int radius)
{
    int halite = 0;

    for (int x = tile_position.x - radius; x < tile_position.x + radius; x++)
    {
        for (int y = tile_position.y - radius; y < tile_position.y + radius; y++)
        {
            Position cellPos = {x, y};
            MapCell *cell = game.game_map->at(cellPos);
            halite += cell->halite;
        }
    }
    return halite;
}

/**
 * @brief Finds the closest deposit (shipyard or dropOff) to a given position.
 * 
 * @param from 
 * @param game 
 * @return Position 
 */
Position ClosestDeposit(Position from, Game &game)
{

    Position closest_position = game.me->shipyard->position;
    int closest_distance = game.game_map->calculate_distance(from, game.me->shipyard->position);

    for (const auto &dropoff_iterator : game.me->dropoffs)
    {

        shared_ptr<Dropoff> dropoff = dropoff_iterator.second;

        int distance = game.game_map->calculate_distance(from, dropoff->position);

        if (distance <= closest_distance)
        {
            closest_distance = distance;
            closest_position = dropoff->position;
        }
    }
    return closest_position;
}

/**
 * @brief Finds the best position for a new dropOff in a given radius.
 * 
 * @param game 
 * @param shipYardPos 
 * @param radius 
 * @return Position 
 */
Position BestDropOffPosition(Game &game, Position shipyard_pos, int radius)
{
    int best_halite = 0;
    Position best_halite_tile;

    for (int x = shipyard_pos.x - radius; x <= shipyard_pos.x + radius; x++)
    {
        for (int y = shipyard_pos.y - radius; y <= shipyard_pos.y + radius; y++)
        {
            Position cell_pos = {x, y};

            // excludes occupied cells
            if (game.game_map->at(cell_pos)->has_structure())
                continue;

            // excludes the cell if it's too close to another structure
            if (game.game_map->calculate_distance(cell_pos, ClosestDeposit(cell_pos, game)) < BUILDING_SPACING)
                continue;

            int average_halite = TileAverageHalite(game, cell_pos, 4);

            if (average_halite > best_halite)
            {
                best_halite = average_halite;
                best_halite_tile = cell_pos;
            };
        }
    }
    return best_halite_tile;
}

/**
 * @brief Finds the closest ships to a given position.
 * 
 * @param game 
 * @param pos 
 * @param Ships 
 * @return shared_ptr<Ship> 
 */
shared_ptr<Ship> ClosestShipToPosition(Game &game, Position pos, std::unordered_map<EntityId, std::shared_ptr<Ship>> Ships)
{
    int closest_distance = 999;

    shared_ptr<Ship> closest_ship;

    for (const auto &ship_iterator : Ships)
    {
        shared_ptr<Ship> ship = ship_iterator.second;

        int distance = game.game_map->calculate_distance(pos, ship->position);

        if (game.game_map->calculate_distance(pos, ship->position) < closest_distance)
        {
            closest_distance = distance;
            closest_ship = ship;
        };
    }

    return closest_ship;
}
