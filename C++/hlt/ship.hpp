#pragma once

#include "entity.hpp"
#include "constants.hpp"
#include "command.hpp"

#include <memory>

namespace hlt {
    struct Ship : Entity {
        Position posTarget;
        Position lastPos;
        int purpose;
        Halite halite;
        bool goingBack;

        Ship(PlayerId player_id, EntityId ship_id, int x, int y, Halite halite) :
            Entity(player_id, ship_id, x, y),
            halite(halite)
        {}


        enum class Purpose
        {
            Stuck = 0,          // must stay put b/c too little halite
            Ram = 1,            // trying to hit an adjacent enemy
            EvadeReturner = 2,  // getting out of the way of someone with the Return purpose
            Flee = 3,           // moving away from an adjacent enemy
            Return = 4,         // bringing halite back to base
            Mine = 5,           // getting halite
        };


        bool reachedDestination() { return posTarget.x == position.x && posTarget.y == position.y; }

        bool is_full() const {
            return halite >= constants::MAX_HALITE;
        }

        Command make_dropoff() const {
            return hlt::command::transform_ship_into_dropoff_site(id);
        }

        Command move(Direction direction) const {
            return hlt::command::move(id, direction);
        }

        Command stay_still() const {
            return hlt::command::move(id, Direction::STILL);
        }

        std::string to_string() const {
            return "ID : " + std::to_string(id) + " Position : " + position.to_string() + " Halite : " + std::to_string(halite);
        }




        static std::shared_ptr<Ship> _generate(PlayerId player_id);
    };
}
