#!/usr/bin/env bash

set -e

cmake .
make
./halite --replay-directory replays/ -vvv --width 64 --height 64 "./MyBot" "./MyBot"
